﻿using asp.net.exam.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asp.net.exam.Models
{
    public class Repository : IRepository
    {
        private readonly ApplicationDbContext _context;

        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool BookExists(int id)
        {
            return _context.Book.Any(e => e.Id == id);
        }

        public Book Create(Book book)
        {
            _context.Book.Add(book);
            _context.SaveChanges();
            return book;
        }

        public Book Delete(Book book)
        {
            _context.Book.Remove(book);
            _context.SaveChanges();
            return book;
        }

        public Book Edit(Book book)
        {
            _context.Book.Update(book);
            _context.SaveChanges();
            return book;
        }

        public Book Get(int id)
        {
            var book = _context.Book.FirstOrDefault(x => x.Id == id);
            return book;
        }

        public IList<Book> GetAll()
        {
            return _context.Book.ToList();
        }
    }
}
