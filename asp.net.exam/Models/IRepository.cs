﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace asp.net.exam.Models
{
    public interface IRepository
    {
        Book Create(Book book);

        Book Get(int id);
        IList<Book> GetAll();

        Book Edit(Book book);
        Book Delete(Book book);

        bool BookExists(int id);


    }
}
