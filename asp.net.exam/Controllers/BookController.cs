﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using asp.net.exam.Data;
using asp.net.exam.Models;

namespace asp.net.exam.Controllers
{
    public class BookController : Controller
    {
        private readonly IRepository _repository;

        public BookController(IRepository repository)
        {
            _repository = repository;
        }

        // GET: Book
        public IActionResult Index()
        {
            var books = _repository.GetAll();
            return View(books);
        }

        // GET: Book/Details/5
        public IActionResult Details(int id)
        {
            if (_repository.Get(id) == null)
            {
                return NotFound();
            }

            return View(_repository.Get(id));
        }

        // GET: Book/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Book/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Book book)
        {
            if (ModelState.IsValid)
            {

                _repository.Create(book);
                return RedirectToAction(nameof(Index));
            }
            return View(book);
        }

        // GET: Book/Edit/5
        public IActionResult Edit(int id)
        {
            if (_repository.Get(id) == null)
            {
                return NotFound();
            }

            var book = _repository.Get(id);
            return View(book);
        }

        // POST: Book/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, Book book)
        {
            if (id != book.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _repository.Edit(book);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(book);
        }

        // GET: Book/Delete/5
        public IActionResult Delete(int id)
        {
            if (_repository.Get(id) == null)
            {
                return NotFound();
            }

            var book = _repository.Get(id);
            return View(book);
        }

        // POST: Book/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var book = _repository.Get(id);
            _repository.Delete(book);
            return RedirectToAction(nameof(Index));
        }

        private bool BookExists(int id)
        {
            return _repository.BookExists(id);
        }

        [HttpPost]
        public JsonResult GiveOut(int Count, int Id)
        {
            var book = _repository.Get(Id);
            if (Count == 0)
            {
                book.Count = 0;
            }
            else {
                book.Count = --Count;
                _repository.Edit(book);
            }    
            return Json(book.Count);
        }

    }
}
